# paleofetch-bsd

A rewrite of [neofetch](https://github.com/dylanaraps/neofetch) started by [ss7m for linux](https://github.com/ss7m/paleofetch).

Not much code was left, but name is beautiful and similar logic.

## System support

* macOS Aarch64, x86\_64(not tested)
* NetBSD
* FreeBSD

For errors relating BSDs see [TroubleshootCustomise.md](TroubleshootCustomise.md)

## Why use paleofetch over neofetch?

One major reason is the performance improvement.

At least for now paleofetch execution time is around 0.05s while neofetch will execute at least a second on macOS. The difference is much smaller on BSDs, but with caching it's still a lot quicker.

![example output](.gitlab/example.png)

## Recaching

Paleofetch uses caching to run faster. To recache data run `paleofetch -r`.

## Customisation

Paleofetch uses "suckless" like customisation. If you for example don't want GPU to show, you have to edit `src/config.h`(there are two defines, first for macOS, second for all the others, and comment the line with `/* */`. To see example and read more see [TroubleshootCustomise.md](TroubleshootCustomise.md)

## Compiling

Enter folder and type `make` (can be bmake, gnumake also)

There is also option `make clean` to clean.

## FAQ

**Q**: Do you really run neofetch every time you open a terminal?  

**ss7m**: Yes, I like the way it looks and like that it causes my prompt to start midway
down the screen. I do acknowledge that the information it presents is not actually useful.

**DB**: Yea, me too that's why I forked it.
