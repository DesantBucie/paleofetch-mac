## TROUBLESHOTING

* X11 error
    1. You aren't using Xorg server - modify config so it doesn't contain get\_gpu and get\_resolution.
    2. You don't have Xorg installed at all - modify makefile variable from `NETBSD_OBJ=netbsd.o -R/usr/X11R7/lib -L /usr/X11R7/lib -lX11 -lGL` to just `NETBSD_OBJ=netbsd.o`. The same goes for FreeBSD.

* Invalid MIT-MAGIC-COOKIE-1. 
    For some reason using remote desktop and changing user after login, will cause the problem. You can only run paleofetch using the user you logged with in that case.

## CUSTOMISATION

To customise paleofetch you will have to recompile every time you do a change.

### Let's give it a try

Let's say you are on the desktop and you are tired of seeing "Unknown" in battery section.
Open file `src/config.h`. You'll have to comment a line with `get_battery()` as shown below.

```c
#define CONFIG \
{ \
    /* name            function                 cached  */\
    { "",             get_user_and_host,        true  }, \
    { "",             hostname_underline,       true  }, \
	{ "Shell: ",      get_shell,                true  }, \
	{ "OS: ",	      get_complete_os,          true  }, \
	{ "Kernel: ",     get_kernel,               true  }, \
    { "Machine: ",    get_machine,              true  }, \
	{ "Uptime: ",     get_uptime,               false }, \
	{ "Resolution: ", get_resolution,           true  }, \
	{ "Terminal: ",   get_terminal,             false }, \
    /* Comment like below */ \
    /*{ "Battery: ",  get_battery_procentage,   false }, */ \
	{ "RAM: ",        get_ram_usage,	        false }, \
	{ "CPU: ",        get_cpu,                  true  }, \
	{ "GPU: ",        get_gpu,                  true  }, \
	SPACER \
	{ "",             get_colors1,              true  }, \
	{ "",             get_colors2,              true  }, \
}
```
#### You have to do it exacly with `/* */`, because `//` will comment the rest of the define.

**Cached true** means value will be kept in cache - good for PC components, not good for time, battery etc. 
**Cached false** will be exact oposite, value will be updated every time program runs.
