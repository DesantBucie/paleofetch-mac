OBJ=paleofetch.o sysctl_info.o
CFLAGS=-Wall -Wextra -Wpedantic
CC=cc
OPT=-O3
TARGET=/usr/local/bin/
MAC_OBJ=macintosh.o
NETBSD_OBJ=netbsd.o -R/usr/X11R7/lib -L /usr/X11R7/lib -lX11 -lGL
NETBSD_X11=-I/usr/X11R7/include
FREEBSD_OBJ= netbsd.o -R/usr/local/lib -L /usr/local/lib -lX11 -lGL
FREEBSD_X11=-I/usr/local/include
FRAMEWORKS=-framework Cocoa -framework IOKit -framework CoreFoundation

paleofetch: clean $(OBJ)
	@if [ "$$(uname -s)" = "Darwin" ]; then \
		$(CC) ${CFLAGS} $(OPT) -c src/macintosh.c; \
		$(CC) $(OBJ) $(MAC_OBJ) $(FRAMEWORKS) -o $@; \
	elif [ "$$(uname -s)" = "NetBSD" ]; then \
		$(CC) ${CFLAGS} $(OPT) $(NETBSD_X11) -c src/netbsd.c; \
		$(CC) $(OBJ) $(NETBSD_OBJ) -o $@; \
	elif [ "$$(uname -s)" = "FreeBSD" ]; then \
		$(CC) ${CFLAGS} $(OPT) $(FREEBSD_X11) -c src/netbsd.c; \
		$(CC) $(OBJ) $(FREEBSD_OBJ) -o $@; \
	else \
		$(CC) $(OBJ) -o $@; \
	fi ;
install: paleofetch
	@mkdir -p $(TARGET)
	@if [ -x "$$(command -v doas)" ]; then \
		doas cp paleofetch $(TARGET); \
	else \
		sudo cp paleofetch $(TARGET); \
	fi
clean:
	@echo "Cleaning..."
	@rm -f paleofetch a.out *.o

%.s: src/%c
	$(CC) -S $@ $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(OPT) -c $<

sysctl_info.o: src/sysctl_info.c src/sysctl_info.h
paleofetch.o: src/paleofetch.c src/paleofetch.h src/config.h
macintosh.o: src/macintosh.c src/macintosh.h
netbsd.o: src/netbsd.c src/netbsd.h
