#ifndef NETBSD_LOGO_H
#define NETBSD_LOGO_H

const int logo_line_length = 32;
const char *logo[] = { 
"\033[38;5;166;1m                     `-/oshdmNMNdhyo+:-` ",
"\033[1my/\033[38;5;166ms+:-``    `.-:+oydNMMMMNhs/-``         ",
"\033[1m-m+\033[38;5;166mNMMMMMMMMMMMMMMMMMMMNdhmNMMMmdhs+/-`  ",
"\033[1m -m+\033[38;5;166mNMMMMMMMMMMMMMMMMMMMMmy+:`           ",
"\033[1m  -N/\033[38;5;166mdMMMMMMMMMMMMMMMds:`                ",
"\033[1m   -N/\033[38;5;166mhMMMMMMMMMmho:`                    ",
"\033[1m    -N/\033[38;5;166m-:/++/:.`                         ",
"\033[1m     :M+                                 ",
"\033[1m      :Mo                                ",
"\033[1m       :Ms                               ",
"\033[1m        :Ms                              ",
"\033[1m         :Ms                             ",
"\033[1m          :Ms                            ",
"\033[1m           :Ms                           ",
"\033[1m            :Ms                          ",
"\033[1m             :Ms                         ",
"\033[1m              :Ms                        ",
};

#endif
