#if defined(__MACH__) || defined(__APPLE__)
#define CONFIG \
{ \
    /* name            function                 cached  */\
    { "",             get_user_and_host,        true  }, \
    { "",             hostname_underline,       true  }, \
	{ "Shell: ",      get_shell,                true  }, \
	{ "OS: ",		  get_complete_os,          true  }, \
	{ "Kernel: ",     get_kernel,               true  }, \
    { "Machine: ",    get_machine,              true  }, \
	{ "Resolution: ", get_resolution,           true  }, \
	{ "Uptime: ",     get_uptime,               false }, \
	{ "Terminal: ",   get_terminal,             false }, \
    { "Battery: ",    get_battery_procentage,   false }, \
	{ "RAM: ", 		  get_ram_usage,		    false }, \
	{ "CPU: ",        get_cpu,                  true  }, \
	{ "GPU: ",        get_gpu,                  true  }, \
	{ "IP: ",         get_ip,                   false  }, \
	SPACER \
	{ "",             get_colors1,              true  }, \
	{ "",             get_colors2,              true  }, \
}
#else 
#define CONFIG \
{ \
    /* name            function                 cached  */\
    { "",             get_user_and_host,        true  }, \
    { "",             hostname_underline,       true  }, \
	{ "Shell: ",      get_shell,                true  }, \
	{ "OS: ",		  get_complete_os,          true  }, \
    { "Machine: ",    get_machine,              true  }, \
	{ "Resolution: ", get_resolution,           true  }, \
	{ "Uptime: ",     get_uptime,               false }, \
	{ "Terminal: ",   get_terminal,             false }, \
	{ "RAM: ", 		  get_ram_usage,		    false }, \
	{ "CPU: ",        get_cpu,                  true  }, \
	{ "GPU: ",        get_gpu,                  true  }, \
	{ "IP: ",         get_ip,                   false  }, \
	SPACER \
	{ "",             get_colors1,              true  }, \
	{ "",             get_colors2,              true  }, \
}
#endif
