#ifndef NETBSD_H
#define NETBSD_H

#include <machine/cpu.h>
#include <sys/time.h>

void get_ram_usage(char *),
     get_cpu(char *),
     get_complete_os(char *),
     get_gpu(char *),
     get_resolution(char *);

#endif //NETBSD_H
