#include "netbsd.h"
#include "sysctl_info.h"

#include <stdint.h>
#include <string.h>
#include <sys/sysctl.h>

#include <GL/glx.h>
#include <X11/Xlib.h>

#ifdef __FreeBSD__
#include <vm/vm_param.h>
// for struct vmtotal
#include <sys/vmmeter.h>
#endif


extern struct utsname details;


void get_cpu(char *cpu){
    char *restrict model = get_sysctl_info(CTL_HW, HW_MODEL);
    if(model != NULL)
        strlcpy(cpu, model, BUFF_256);
    else
        strcpy(cpu, "Unknown");
    free(model);
}
void get_complete_os(char *restrict os){
    snprintf(os, BUFF_512, "%s %s", details.sysname, details.release);
}
void get_resolution(char *resolution){
    Display *display = XOpenDisplay(NULL); 
    if(!display){
       halt_and_catch_fire("X11 error", 127);
    }
    Screen *screen = DefaultScreenOfDisplay(display);
    int width = WidthOfScreen(screen);
    int height = HeightOfScreen(screen);
    snprintf(resolution, BUFF_256, "%dx%d", width, height);
    free(screen);
    free(display);
}
void get_gpu(char *gpu){

    //Thanks appleman
    //https://opensource.apple.com/source/X11/X11-0.30.4/xc/programs/glxinfo/glxinfo.c.auto.html
    
    Window win;
    GLXContext ctx;
    unsigned long mask;
    XVisualInfo *visinfo;
    Window root;
    XSetWindowAttributes attr;
    int width = 100, height = 100;

    Display *dpy = XOpenDisplay(NULL);
    if(!dpy){
        halt_and_catch_fire("Unable to open screen", 127);
    }

    int scrnum = DefaultScreen(dpy);
    int attribSingle[] = {
        GLX_RGBA,
        GLX_RED_SIZE, 1,
        GLX_GREEN_SIZE, 1,
        GLX_BLUE_SIZE, 1,
        None 
    };
    int attribDouble[] = {
        GLX_RGBA,
        GLX_RED_SIZE, 1,
        GLX_GREEN_SIZE, 1,
        GLX_BLUE_SIZE, 1,
        GLX_DOUBLEBUFFER,
        None 
    };
    root = RootWindow(dpy, scrnum);
    visinfo = glXChooseVisual(dpy, scrnum, attribSingle);
       if (!visinfo) {
          visinfo = glXChooseVisual(dpy, scrnum, attribDouble);
          if (!visinfo) {
             fprintf(stderr, "Error: couldn't find RGB GLX visual\n");
             free(dpy);
             return;
          }
       }
    attr.background_pixel = 0;
    attr.border_pixel = 0;
    attr.colormap = XCreateColormap(dpy, root, visinfo->visual, AllocNone);
    attr.event_mask = StructureNotifyMask | ExposureMask;
    mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;
    win = XCreateWindow(dpy, root, 0, 0, width, height,
                   0, visinfo->depth, InputOutput,
                   visinfo->visual, mask, &attr);
    ctx = glXCreateContext(dpy, visinfo, NULL, 1);
    if(!ctx){
        XDestroyWindow(dpy, win);
        halt_and_catch_fire("OpenGL error", 127);
    }
    if (glXMakeCurrent(dpy, win, ctx)){
        const char *g = (const char *) glGetString(GL_RENDERER);
        strlcpy(gpu, g, BUFF_256);
    }
    else {
        strlcpy(gpu, "Unknown", BUFF_256);
    }
    glXDestroyContext(dpy, ctx);
    XDestroyWindow(dpy, win);
}
#ifdef __FreeBSD__
void get_ram_usage(char *restrict ram_usage)
{
    int64_t *restrict ram_size = (int64_t *) get_sysctl_info(CTL_HW, HW_PHYSMEM);
    uint const ram_size_short = *ram_size >> 20;
    free(ram_size);

    int *pagesize =(int *) get_sysctl_info(CTL_HW, HW_PAGESIZE);

    struct vmtotal *v = get_sysctl_info(CTL_VM, VM_TOTAL);

    int64_t mem_free = (v->t_free * *pagesize >> 20);
    int used_memory = ram_size_short - mem_free;

    free(v);



    snprintf(ram_usage, BUFF_64, "%dMB/%dMB %c%d%s",
            used_memory, ram_size_short, '(', used_memory * 100/(ram_size_short != 0 ? ram_size_short : 1) , "%)");
}
#endif
#ifdef __NetBSD__
void get_ram_usage(char *ram_usage){
    int64_t *ram_size = (int64_t *) get_sysctl_info(CTL_HW, HW_PHYSMEM64);
    int const ram_size_short = *ram_size >> 20;
    free(ram_size);
    char command[] = "awk '/MemFree:/{f=$2} f{print f / 1024 ; exit}' /proc/meminfo";
    char ref[BUFF_64] = {'\0'};
    execute_command(command, ref);
    int used_mem = ram_size_short - atoi(ref);

    snprintf(ram_usage, BUFF_256, "%dMB/%dMB (%d%c)",
            used_mem,
            ram_size_short,
            used_mem * 100 / ram_size_short, 
            '%');
}
#endif
